{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}

module Data.Buffering.Test (syncedBuffers, SyncedBuffers (..)) where

import Control.Arrow ((>>>))
import Control.Concurrent.STM
  ( isEmptyTChan,
    newTChanIO,
    newTVarIO,
    readTChan,
    stateTVar,
    writeTChan, newEmptyTMVarIO, readTMVar, putTMVar, readTVar, writeTVar,
  )
import Data.Buffering
  ( BufferIn,
    BufferOut,
    BufferOutLog (BufferOutLogFailure, BufferOutLogSuccess),
  )
import qualified Data.Vector as Vector
import Protolude hiding ((<.>))
import Streaming (Of ((:>)), chunksOf)
import qualified Streaming.Prelude as S
import System.FilePath ((<.>))

-- | buffers that use memory as a backend, useful for testing
data SyncedBuffers f s r a = SyncedBuffers
  { -- | blocking action that you can interleave in your tests
    -- if you need the buffers queue empty before the next step
    syncing :: IO (),
    -- | instruct the bufferOut to exit
    exitBufferOut :: IO (),
    -- | matches the real one but write to memory and not to disk
    syncedBufferIn :: forall m . MonadIO m => BufferIn m a r,
    -- | will block and will never exit (do not
    --  reschedule, or make sure the rescheduling system is guarded)
    syncedBufferOutV :: forall m . MonadIO m => BufferOut m f s a
  }

-- | create the STM cells and link the buffers to use it
syncedBuffers :: IO (SyncedBuffers f s r a)
syncedBuffers = do
  -- vector channel
  chan <- newTChanIO
  -- buffer count
  count <- newTVarIO (0 :: Int)
  -- exit bufferout
  exit <- newEmptyTMVarIO
  -- flag: have we finished processing all outstanding elements?
  synchronized <- newTVarIO True
  let block =
        atomically $ guard =<< readTVar synchronized
      bufferIn fp n s =
        s & do
          chunksOf n >>> S.mapped
            \input -> do
              (v :> x) <- S.toList input
              name <- liftIO $
                atomically $ do
                  !j <- stateTVar count $ \j -> (j, j + 1)
                  let name = fp <.> show j
                  writeTChan chan (name, Vector.fromList v)
                  writeTVar synchronized False
                  pure name
              pure (name :> x)
      bufferOut _fp action = go
        where
          go = do
            next <- liftIO $ atomically $ (Just <$> readTChan chan) <|> (Nothing <$ readTMVar exit)
            case next of
              Nothing -> pure ()
              Just (fp, v) -> do
                x <- lift $ action v
                S.yield $
                  either (BufferOutLogFailure fp) (BufferOutLogSuccess fp) x
                liftIO . atomically $ do
                  writeTVar synchronized =<< isEmptyTChan chan
                go
      doExitBufferOut = atomically $ putTMVar exit ()

  pure $ SyncedBuffers block doExitBufferOut bufferIn bufferOut
