{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Avoid lambda using `infix`" #-}

module Data.Buffering (
  BufferIn,
  BufferOut,
  BufferOutLog (..),
  _BufferOutLogDecodingFailed,
  _BufferOutLogSuccess,
  _BufferOutLogFailure,
  _BufferOutLogDeleteFailure,
  saveVectorToNextFileIn,
  saveVectorToFile,
  bufferIn,
  readVectorFromFile,
  bufferOutV,
  bufferOutVS,
  bufferOutVAbortOnLeft,
  bufferOutAny,
  bufferOut,
  countPositives,
  BufferOutCounts (..),
  bufferOutCnts,
) where

import Control.Foldl qualified as L
import Control.Lens ( makePrisms )
import Control.Monad.Catch ( bracket, catchAll )
import Data.ByteString qualified as BS
import Data.Serialize (Serialize, decode, encode)
import Data.String (String)
import Data.Time (formatTime, defaultTimeLocale)
import Data.Time.Clock.POSIX (getCurrentTime)
import Data.Vector (Vector)
import Data.Vector qualified as Vector
import Data.Vector.Serialize ()
import Protolude hiding (bracket, catch, (<.>))
import Streaming (MFunctor (hoist), Of ((:>)), Stream, chunksOf, unfold, concats)
import Streaming.Prelude qualified as S
import System.Directory (createDirectoryIfMissing, listDirectory, removeFile, renamePath, doesPathExist)
import System.FilePath ((</>), (<.>))
import System.IO (openBinaryFile, withBinaryFile)
import System.IO.Error (isAlreadyExistsError, mkIOError, alreadyExistsErrorType)
import System.Posix.IO (OpenFileFlags (..), defaultFileFlags, openFd, OpenMode (..), openFdAt, fdToHandle, handleToFd, closeFd)
import System.Posix.Unistd (fileSynchroniseDataOnly, fileSynchronise)

-- | signature of a bufferIn
type BufferIn m a r
  = FilePath -- ^ directory where bufferOut is reading
  -> Int -- ^ vector size to be dumped
  -> Stream (Of a) m r -- ^ input of serializables
  -> Stream (Of FilePath) m r -- ^ files produced FilePath

-- | signature of a bufferOut
type BufferOut m f s a
  = FilePath -- ^ directory where bufferIn is writing
  -> (Vector a -> m (Either f s)) -- ^ what to do with each consumed vector, returning Left will not remove the file
  -> Stream (Of (BufferOutLog f s)) m ()


tempFileSuffix :: FilePath
tempFileSuffix = ".part~"

-- | This function writes a vector of data to a new file under the specified
-- directory and returns the name of the created file.
--
-- This function is safe to call from multiple threads or processes on the same
-- directory (but doing so is not recommended). When the function returns, the
-- data is guaranteed to be written to disk (subject to file system and
-- hardware limitations).
saveVectorToNextFileIn :: (Serialize a) => FilePath -> Vector a -> IO FilePath
saveVectorToNextFileIn dfp v = do
    -- step 1: open a descriptor for the target directory
    bracket
      do openFd dfp ReadOnly defaultFileFlags{ directory = True }
      do closeFd
      $ \dfd -> do
        -- step 2: generate a unique filename and return a file descriptor for it
        bracket
          -- Generate a timestamp TS and create an intermediate file ("<TS>.part~") for it.
          -- If "<TS>.part~" already exists, openFdAt fails with an
          -- alreadyExistsError, which we catch (then we sleep 1 microsecond
          -- and try again). This case would indicate that there is a
          -- concurrent writer that was scheduled at exactly the same time to
          -- generate a file.
          -- If "<TS>.part~" was created successfully (indicating that we are
          -- currently the only writer using that particular timestamp), check
          -- whether "<TS>" exists. (If so, another writer has already finished
          -- generating a file at that particular instant.) Delete our
          -- "<TS>.part~" file and try again (by throwing a synthetic
          -- alreadyExistsError).
          -- Otherwise (if "<TS>.part~" could be created and "<TS>" does not
          -- exist), we are the canonical owner of this timestamp. Return the
          -- file descriptor for "<TS>.part~" as well as the names of the.
          -- intermediate file ("<TS>.part~") and the finished file ("<TS>").
          do retryEEXIST (threadDelay 1) $ do
              fp_proto <- getCandidateName
              let fp_partial = fp_proto <.> tempFileSuffix
              bracketOnError
                do openFdAt (Just dfd) fp_partial WriteOnly defaultFileFlags{ creat = Just 0o666, exclusive = True }
                do closeFd
                $ \fd -> do
                  let fp_final = dfp </> fp_proto
                  fpExists <- doesPathExist fp_final
                  when fpExists $ do
                    removeFile fp_partial
                    ioError $ mkIOError alreadyExistsErrorType "saveVectorToNextFileIn" Nothing (Just fp_final)
                  pure (fp_final, dfp </> fp_partial, fd)
          do \(_, _, fd) -> closeFd fd
          $ \(fp_final, fp_partial, fd) -> do
            -- step 3: write data to file
            bracket
              do fdToHandle fd
              do handleToFd  -- handleToFd automatically flushes buffers for us
              $ \h -> do
                BS.hPut h $ encode v
            -- At this point all available data has been written to
            -- "<TS>.part~". Call fdatasync() to ensure the file contents are on
            -- disk.
            fileSynchroniseDataOnly fd
            -- ... and rename "<TS>.part~" to "<TS>"
            renamePath fp_partial fp_final
            -- ... and call fsync() on the containing directory, ensuring that
            -- the directory entries are all in good order
            fileSynchronise dfd
            pure fp_final
  where
    -- use padding to ensure filenames are generated in ascending order, no
    -- matter how many digits there are in the current timestamp ("1000" <
    -- "999", but "000999" < "001000")
    getCandidateName :: IO FilePath
    getCandidateName = formatTime defaultTimeLocale "A%12s.%9q" <$> getCurrentTime

    retryEEXIST :: IO () -> IO a -> IO a
    retryEEXIST delay action = loop
      where
        loop = do
          catchJust (guard . isAlreadyExistsError) action $ \() -> do
            delay
            loop

saveVectorToFile :: (Serialize a) => FilePath -> Vector a -> IO ()
saveVectorToFile fp v = bracket
    do openBinaryFile fp_partial WriteMode
    do \h ->
        bracket (handleToFd h) closeFd $ \fd -> do
          fileSynchroniseDataOnly fd
          renamePath fp_partial fp
    do \h -> BS.hPut h $ encode v
  where
    fp_partial = fp <.> tempFileSuffix

bufferIn
  :: forall a m r.
  (MonadIO m, Serialize a)
  => BufferIn m a r
bufferIn fp n s = do
  liftIO $ createDirectoryIfMissing True fp
  s & chunksOf n
    & S.mapped
      \input -> do
        (v :> x) <- S.toList input
        name <- liftIO $ saveVectorToNextFileIn fp $ Vector.fromList v
        pure (name :> x)

-- | stream directory contents sorted by name, skipping over @.part~@ files
directoryContent :: FilePath -> Stream (Of FilePath) IO ()
directoryContent fp = do
  fs <- liftIO $ listDirectory fp
  S.each . map (fp </>) . sort . filter (not . (tempFileSuffix `isSuffixOf`)) $ fs

readVectorFromFile
  :: Serialize a
  => FilePath -- ^ file path to read
  -> IO (Either String (Vector a)) -- ^ error message or decoded vector
readVectorFromFile fp =
  withBinaryFile fp ReadMode $
    fmap decode . BS.hGetContents

-- | progress in buffering out
data BufferOutLog f s
  = BufferOutLogDecodingFailed FilePath String
  | BufferOutLogSuccess FilePath s
  | BufferOutLogFailure FilePath f
  | BufferOutLogDeleteFailure FilePath SomeException
  deriving (Show)

makePrisms ''BufferOutLog

-- | low level buffering out with access to each vector read, a log of each vector operation is reported
bufferOutV
  :: forall a f s m.
  (MonadIO m, Serialize a)
  => FilePath -- ^ directory where bufferIn is writing
  -> (Vector a -> m (Either f s)) -- ^ what to do with each consumed vector, returning Left will not remove the file
  -> Stream (Of (BufferOutLog f s)) m ()
bufferOutV dir commit = bufferOutVS dir (S.yield <=< lift . commit)

-- | low level buffering out with access to each vector read; the logs of each vector operation are reported
bufferOutVS
  :: forall a f s m.
  (MonadIO m, Serialize a)
  => FilePath -- ^ directory where bufferIn is writing
  -> (Vector a -> Stream (Of (Either f s)) m ()) -- ^ what to do with each consumed vector, returning at least one Left will not remove the file
  -> Stream (Of (BufferOutLog f s)) m ()
bufferOutVS dir commit = do
    liftIO $ createDirectoryIfMissing True dir
    hoist liftIO (directoryContent dir) & flip S.for (\fp -> processFileAsVector fp commit)

processFileAsVector
  :: (MonadIO m, Serialize a)
  => FilePath
  -> (Vector a -> Stream (Of (Either f s)) m ())
  -> Stream (Of (BufferOutLog f s)) m ()
processFileAsVector fp commit = do
  r <- liftIO $ readVectorFromFile fp
  case r of
    Left s -> S.yield $ BufferOutLogDecodingFailed fp s
    Right vec -> do
      allRight <- S.map eitherToBufferOutLog $ S.store (L.purely S.fold_ (L.all isRight)) $ commit vec
      when allRight $ do
        join $ liftIO $ catchAll
          do
            removeFile fp
            pure $ pure ()
          do pure . S.yield . BufferOutLogDeleteFailure fp
  where
  eitherToBufferOutLog (Left f) = BufferOutLogFailure fp f
  eitherToBufferOutLog (Right f) = BufferOutLogSuccess fp f

-- | Like 'bufferOutV' but aborts reading the remaining vectors on first processing failure.
-- Note that 'BufferOutLogDeleteFailure' is not a processing failure
bufferOutVAbortOnLeft
  :: forall a f s m.
  (MonadIO m, Serialize a)
  => FilePath -- ^ directory where bufferIn is writing
  -> (Vector a -> m (Either f s)) -- ^ what to do with each consumed vector, returning Left will not remove the file
  -> Stream (Of (BufferOutLog f s)) m ()
bufferOutVAbortOnLeft dir commit = do
    liftIO $ createDirectoryIfMissing True dir
    hoist liftIO (directoryContent dir) & (concats . unfold attemptProcessing)
  where
    attemptProcessing
      :: Stream (Of FilePath) m ()
      -> m (Either () ((Stream (Of (BufferOutLog f s)) m) (Stream (Of FilePath) m ())))
    attemptProcessing s = do
      xOrEnd <- S.next s
      case xOrEnd of
        Left () -> pure $ Left ()
        Right (fp, rest) -> do
          bufferOutLogs <- S.toList_ $ processFileAsVector fp (S.yield <=< lift . commit)
          case traverse (guarded processingFailed) bufferOutLogs of
            Nothing -> pure $ Left ()
            Just resultLogs -> pure $ Right $ S.each resultLogs $> rest

processingFailed :: BufferOutLog f s -> Bool
processingFailed = \case
  BufferOutLogDecodingFailed _fp _s -> True
  BufferOutLogSuccess _fp _b -> False
  BufferOutLogFailure _fp _a -> True
  BufferOutLogDeleteFailure _fp _a -> False -- processing succeeded in this case!

bufferOutAny
  :: (MonadIO m, Serialize a)
  => BufferOut (Stream (Of a) m) () () a
  -> FilePath -- ^ directory where bufferIn is writing
  -> Stream (Of (BufferOutLog () ())) (Stream (Of a) m) ()
bufferOutAny bufferOutV' dir = bufferOutV' dir $ \v -> do
  S.each . toList $ v
  pure $ Right ()

-- | natural buffering out with reading vector operation on the external layer
bufferOut
  :: (MonadIO m, Serialize a)
  => FilePath -- ^ directory where bufferIn is writing
  -> Stream (Of (BufferOutLog () ())) (Stream (Of a) m) ()
bufferOut = bufferOutAny bufferOutV

countPositives :: Monad m => Stream (Of (BufferOutLog a b)) m () -> m Int
countPositives = L.purely S.fold_ $ L.handles _BufferOutLogSuccess L.length

data BufferOutCounts = BufferOutCounts
  { decodingFailedCnt :: Int
  , successCnt :: Int
  , failureCnt :: Int
  , deleteFailureCnt :: Int
  }
  deriving Show

bufferOutCnts :: Monad m => Stream (Of (BufferOutLog a b)) m () -> m BufferOutCounts
bufferOutCnts = L.purely S.fold_ $
  BufferOutCounts
    <$> L.handles _BufferOutLogDecodingFailed L.length
    <*> L.handles _BufferOutLogSuccess L.length
    <*> L.handles _BufferOutLogFailure L.length
    <*> L.handles _BufferOutLogDeleteFailure L.length

