{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE TupleSections #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}

import Control.Concurrent.STM (newTVarIO, modifyTVar, stateTVar)
import Control.Lens ( (^..) )
import Data.Buffering
    ( bufferIn,
      bufferOut,
      bufferOutV,
      _BufferOutLogSuccess,
      _BufferOutLogDecodingFailed,
      _BufferOutLogFailure,
      bufferOutAny,
      saveVectorToFile,
      readVectorFromFile,
    )
import Data.Buffering.Test (SyncedBuffers(..), syncedBuffers)
import Data.Set qualified as Set
import Data.Vector qualified as Vector
import Protolude
import Streaming (Of ((:>)))
import Streaming.Prelude qualified as S
import System.Directory (doesDirectoryExist, removeDirectoryRecursive, createDirectory)
import System.FilePath ((</>))
import System.IO.Error (isDoesNotExistError)
import System.Random.Stateful (uniformRM, globalStdGen)
import Test.Hspec (describe, hspec, it, shouldBe, shouldSatisfy)

bufferDir :: FilePath
bufferDir = "bufferDir"

deleteBufferDir :: IO ()
deleteBufferDir = handleJust (guard . isDoesNotExistError) pure
  $ do removeDirectoryRecursive bufferDir

main :: IO ()
main = hspec $ do
  describe "synced memory buffering" $ do
    it "pass the lock on no run" $ do
       SyncedBuffers {..} <- syncedBuffers
       syncing
       pure @IO ()
    it "works for one send" $ do
        SyncedBuffers syncing exit bufferIn (bufferOutAny -> bufferOut) <- syncedBuffers
        let xs :: [Int] = [1 .. 10_000]
        ars <- async $ S.toList_ $ S.effects $ bufferOut bufferDir
        _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
        rs <- syncing >> exit >> wait ars
        rs `shouldBe` xs
    it "works for 2 sends" $ do
        SyncedBuffers syncing exit bufferIn (bufferOutAny -> bufferOut) <- syncedBuffers
        ars <- async $ S.toList_ $ S.effects $ bufferOut bufferDir
        let xs :: [Int] = [1 .. 10_000]
        _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
        let ys :: [Int] = [10_001 .. 20_000]
        _ <- S.effects $ bufferIn bufferDir 100 $ S.each ys
        rs <- syncing >> exit >> wait ars
        rs `shouldBe` (xs <> ys)
    it "works for 2 sends with syncing" $ do
        SyncedBuffers syncing exit bufferIn bufferOut <- syncedBuffers
        output <- newTVarIO []
        ao <- async $ S.effects $ bufferOut bufferDir
            $ \v -> do
              threadDelay 1_000
              atomically $ modifyTVar output (<> [v])
              pure $ Right ()
        let xs :: [Int] = [1 .. 10_000]
        _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
        rs' <- syncing >> atomically (stateTVar output (,[]))
        let ys :: [Int] = [10_001 .. 20_000]
        _ <- S.effects $ bufferIn bufferDir 100 $ S.each ys
        rs'' <- syncing >> atomically (stateTVar output (,[]))
        exit >> wait ao
        ((rs' >>= toList) <> (rs'' >>= toList)) `shouldBe` (xs <> ys)
  describe "disk buffering" $ do
    it "supports single-file access without retry or race conditions" $ do
      deleteBufferDir
      createDirectory bufferDir
      let fp = bufferDir </> "single.cache"
          universe =
            let k = 5 in
            [ Vector.fromList (replicate (k - i `mod` k) i) | i <- [1 .. 1_000] ]
      saveVectorToFile @Int fp Vector.empty
      aw <- async $ do
        for_ universe $ \v -> do
          threadDelay =<< uniformRM (1, 100) globalStdGen
          saveVectorToFile @Int fp v
      ars <- replicateM 5_000 $ do
        async $ do
          threadDelay =<< uniformRM (1, 500_000) globalStdGen
          readVectorFromFile @Int fp
      w <- first displayException <$> waitCatch aw
      w `shouldBe` Right ()
      let rightSet = Set.fromList (Right . Right <$> Vector.empty : universe)
      for_ ars $ \ar -> do
        r <- first displayException <$> waitCatch ar
        r `shouldSatisfy` (`Set.member` rightSet)
    it "works synchronously " $ do
      deleteBufferDir
      let xs :: [Int] = [1 .. 10_000]
      _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
      rs <- S.toList_ $ S.effects $ bufferOut bufferDir
      rs `shouldBe` xs
    it "works asynchronously " $ do
      deleteBufferDir
      let xs :: [Int] = [1 .. 10_000]
      w <- async do
        S.effects $ bufferIn bufferDir 100 $ S.each xs
      let r = S.effects $ bufferOut bufferDir
          go = do
            t <- lift $ doesDirectoryExist bufferDir
            if not t
              then lift (threadDelay 10_000) >> go
              else do
                z <- lift $ poll w
                case z of
                  Nothing -> r >> go
                  Just _ -> r
      rs <- S.toList_ go
      rs `shouldBe` xs
    it "leaves failed buffer-out vectors in the buffer" do
      deleteBufferDir
      let xs :: [Int] = [1 .. 10_000]
      _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
      _ <- S.effects $ bufferOutV @Int bufferDir (const $ pure $ Left ())
      rs <- S.toList_ $ S.effects $ bufferOut bufferDir
      rs `shouldBe` xs
    it "logs buffers-out operations" $ do
      deleteBufferDir
      let xs :: [Int] = [1 .. 10_000]
      _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
      rs :> ls <- S.toList $ S.toList_ $ bufferOut bufferDir
      rs `shouldBe` xs
      length (ls ^.. traverse . _BufferOutLogSuccess) `shouldBe` 100
    it "logs failed buffers-out operations" $ do
      deleteBufferDir
      let xs :: [Int] = [1 .. 10_000]
      _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
      ls <- S.toList_ $ bufferOutV @Int bufferDir (const $ pure $ Left ())
      length (ls ^.. traverse . _BufferOutLogFailure ) `shouldBe` 100
    it "logs failed to decode buffers-out operations" $ do
      deleteBufferDir
      let xs :: [Int] = [1 .. 10_000]
      _ <- S.effects $ bufferIn bufferDir 100 $ S.each xs
      ls <- S.toList_ $ bufferOutV @Integer bufferDir (const $ pure $ Left ())
      length (ls ^.. traverse . _BufferOutLogDecodingFailed) `shouldBe` 100
